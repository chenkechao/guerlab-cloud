package net.guerlab.cloud.api.feign;

/**
 * 字段常量
 *
 * @author guer
 */
public interface Constants {

    String FIELD_STATUS = "status";

    String FIELD_ERROR_CODE = "errorCode";

    String FIELD_STACK_TRACES = "stackTraces";

    String FIELD_APPLICATION_NAME = "applicationName";

    String FIELD_STACK_TRACE = "stackTrace";

    String FIELD_DATA = "data";

    String FIELD_MESSAGE = "message";
}
