package net.guerlab.cloud.loadbalancer;

/**
 * 常量
 *
 * @author guer
 */
public interface Constants {

    /**
     * 配置前缀
     */
    String PROPERTIES_PREFIX = "spring.cloud.loadbalancer";

    /**
     * 配置-策略
     */
    String PROPERTIES_POLICY = "policy";
}
